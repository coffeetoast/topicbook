//
//  DataModel.swift
//  TopicBook
//
//  Created by SungJaeLEE on 2016. 1. 4..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import Foundation

class DataModel
{
    var lists = [Topiclist]()
    init() {
        loadTopiclists()
        registerDefaults()
        handleFirstTime()
    }
}

//MARK: - Persistance..........
extension DataModel
{
    func documentsDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        return paths[0]
    }
    
    func dataFilePath() -> String {
        return (documentsDirectory() as NSString).stringByAppendingPathComponent("topiclists.plist")
    }
    
    func saveTopiclists() {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWithMutableData: data)
        archiver.encodeObject(lists, forKey: "topiclists")
        archiver.finishEncoding()
        data.writeToFile(dataFilePath(), atomically: true)
    }
    
    func loadTopiclists() {
        let path = dataFilePath()
        if NSFileManager.defaultManager().fileExistsAtPath(path) {
            if let data = NSData(contentsOfFile: path) {
                let unarchiver = NSKeyedUnarchiver(forReadingWithData: data)
                lists = unarchiver.decodeObjectForKey("topiclists") as! [Topiclist]
                unarchiver.finishDecoding()
                sortChecklists()
            }
        }
    }
}

//MARK: - NSUserDefaults.............
extension DataModel
{
    var indexOfSelectedTopiclist: Int {
        get {
            return NSUserDefaults.standardUserDefaults().integerForKey("TopiclistIndex")
        }
        set {
            NSUserDefaults.standardUserDefaults().setInteger(newValue, forKey: "TopiclistIndex")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    func registerDefaults() {
        let dictionary = ["TopiclistIndex": -1,
                            "FirstTime": true ,
                            "TopiclistItemID": 0]
        
        NSUserDefaults.standardUserDefaults().registerDefaults(dictionary)
    }
    
    func handleFirstTime() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let firstTime = userDefaults.boolForKey("FirstTime")
        if firstTime {
            let topiclist = Topiclist(name: "List")
            lists.append(topiclist)
            indexOfSelectedTopiclist = 0
            userDefaults.setBool(false, forKey: "FirstTime")
            userDefaults.synchronize()
        }
    }
}

//MARK: - SortCheck...........................
extension DataModel
{
    func sortChecklists() {
        lists.sortInPlace { (topiclist1, topiclist2) -> Bool in
            topiclist1.name.localizedStandardCompare(topiclist2.name) == .OrderedAscending
        }
    }
}

//MARK: - Helper.............................
extension DataModel
{
    class func nextTopiclistItemID() -> Int {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let itemID = userDefaults.integerForKey("TopiclistItemID")
        userDefaults.setInteger(itemID + 1, forKey: "TopiclistItemID")
        userDefaults.synchronize()
        return itemID
    }
}