//
//  Topiclist.swift
//  TopicBook
//
//  Created by SungJaeLEE on 2016. 1. 1..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit

final class Topiclist: NSObject
{
    var name = ""
    var iconName: String
    var items = [UrlItem]()
    
    //override init() { super.init() }
    convenience init(name: String) {
        self.init(name: name, iconName: "No Icon")
    }
    
    init(name: String, iconName: String){
        self.name = name
        self.iconName = iconName
        super.init()
    }
}

//MARK: Persistence......
extension Topiclist: NSCoding
{
    convenience init?(coder aDecoder: NSCoder) {
        self.init(name: "No Name")
        name = aDecoder.decodeObjectForKey("Name") as! String
        items = aDecoder.decodeObjectForKey("Items") as! [UrlItem]
        iconName = aDecoder.decodeObjectForKey("IconName") as! String
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(name, forKey: "Name")
        aCoder.encodeObject(items, forKey: "Items")
        aCoder.encodeObject(iconName, forKey: "IconName")
    }
}

//MARK: Show the number of to-do items remaining............. 
extension Topiclist
{
    func countUncheckedItems() -> Int {
        var count = 0
        for item in items where !item.checked {
            count += 1
        }
        return count
    }
}


