//
//  ListDetailViewController.swift
//  TopicBook
//
//  Created by SungJaeLEE on 2016. 1. 1..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit

protocol ListDetailViewControllerDelegate: class
{
    func listDetailViewControllerDidCancel(controller: ListDetailViewController)
    func listDetailViewController(controller: ListDetailViewController, didFinishAddingTopiclist topiclist: Topiclist)
    func listDetailViewController(controller: ListDetailViewController, didFinishEditingTopiclist topiclist: Topiclist)
}

class ListDetailViewController: UITableViewController
{
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var doneBarButton: UIBarButtonItem!
    @IBOutlet weak var iconImageView: UIImageView!
    var iconName = "Folder"
    
    weak var delegate: ListDetailViewControllerDelegate?
 
    var topiclistToEdit: Topiclist?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let topiclist = topiclistToEdit {
            title = "Edit Topiclist"
            textField.text = topiclist.name
            doneBarButton.enabled = true
            iconName = topiclist.iconName
        }
        
        iconImageView.image = UIImage(named: iconName)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        textField.becomeFirstResponder()
    }

    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        if indexPath.section == 1 {
            return indexPath
        } else {
            return nil
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PickIcon" {
            let controller = segue.destinationViewController as! IconPickerViewController
            controller.delegate = self
        }
    }
}

//Cancel or Done Button ...........
extension ListDetailViewController
{
    @IBAction func cancel(sender: AnyObject) {
        delegate?.listDetailViewControllerDidCancel(self)
    }
    
    @IBAction func done(sender: AnyObject) {
        if let topiclist = topiclistToEdit {
            topiclist.name = textField.text!
            topiclist.iconName = iconName
            delegate?.listDetailViewController(self, didFinishEditingTopiclist: topiclist)
        } else {
            let topiclist = Topiclist(name: textField.text!, iconName: iconName)
            delegate?.listDetailViewController(self, didFinishAddingTopiclist: topiclist)
        }
    }
}

//TextFieldDelegate ..............
extension ListDetailViewController: UITextFieldDelegate
{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let oldText: NSString = textField.text!
        let newText: NSString = oldText.stringByReplacingCharactersInRange(range, withString: string)
        doneBarButton.enabled = (newText.length > 0)
        return true
    }
}


//MARK: - IconPickerViewControllerDelegate ................... 
extension ListDetailViewController: IconPickerViewControllerDelegate
{
    func iconPicker(picker: IconPickerViewController, didPickIcon iconName: String) {
        self.iconName = iconName
        iconImageView.image = UIImage(named: iconName)
        navigationController?.popViewControllerAnimated(true)
    }
}