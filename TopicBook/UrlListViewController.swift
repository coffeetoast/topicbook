//
//  UrlListViewController.swift
//  TopicBook
//
//  Created by SungJaeLEE on 2015. 12. 22..
//  Copyright © 2015년 SungJaeLEE. All rights reserved.
//

import UIKit

class UrlListViewController: UITableViewController
{

    var topiclist: Topiclist!
    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        //loadUrllistItems()
//        
//        //print("Data file Path is\(dataFilePath())")
//    }
    
//    func loadUrllistItems() {
//        let path = dataFilePath()
//        if NSFileManager.defaultManager().fileExistsAtPath((path)) {
//            if let data = NSData(contentsOfFile: path) {
//                let unarchiver = NSKeyedUnarchiver(forReadingWithData: data)
//                items = unarchiver.decodeObjectForKey("UrllistItems") as! [UrlItem]
//                unarchiver.finishDecoding()
//            }
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = topiclist.name
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "AddItem" {
            let navigationController = segue.destinationViewController as! UINavigationController
            let controller = navigationController.visibleViewController as! ItemDetailViewController
            
            controller.delegate = self
        } else if segue.identifier == "EditItem" {
            let navigationController = segue.destinationViewController as! UINavigationController
            let controller = navigationController.visibleViewController as! ItemDetailViewController
            controller.delegate = self
            
            if let indexPath = tableView.indexPathForCell(sender as! UITableViewCell) {
                controller.itemToEdit = topiclist.items[indexPath.row]
            }
        }
    }

}


//MARK: - UITableViewDataSource
extension UrlListViewController
{
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topiclist.items.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UrlItem", forIndexPath: indexPath)
        let item = topiclist.items[indexPath.row]
        
        configureTextForCell(cell, withUrlItem: item)
        configureCheckmarkForCell(cell, withUrlItem: item)
        
        return cell
    }
    
    func configureTextForCell(cell: UITableViewCell, withUrlItem item: UrlItem) {
        let label = cell.viewWithTag(1000) as! UILabel
        label.text = item.text
    }
    
    func configureCheckmarkForCell(cell: UITableViewCell, withUrlItem item: UrlItem) {
        
        let label = cell.viewWithTag(1001) as! UILabel
        label.textColor = view.tintColor
        if item.checked {
            label.text = "√"
        } else {
            label.text = ""
        }
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        topiclist.items.removeAtIndex(indexPath.row)
        let indexPaths = [indexPath]
        tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
        
        //saveUrllistItems()
    }
    
}

//MARK: - UITableViewDelegate
extension UrlListViewController
{
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) {
            let item = topiclist.items[indexPath.row]
            item.toggleChecked()
            
            configureCheckmarkForCell(cell, withUrlItem: item)
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        //saveUrllistItems()
    }
    

  
    
}

//MARK: - ItemDetailViewControllerDelegate
extension UrlListViewController: ItemDetailViewControllerDelegate
{
    func itemDetailViewControllerDidCancel(controller: ItemDetailViewController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func itemDetailViewController(controller: ItemDetailViewController, didFinishAddingItem item: UrlItem) {
        let newRowIndex = topiclist.items.count
        
        topiclist.items.append(item)
        
        let indexPath = NSIndexPath(forRow: newRowIndex, inSection: 0)
        let indexPaths = [indexPath]
        tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
        
        dismissViewControllerAnimated(true, completion: nil)
        //saveUrllistItems()
    }
    
    
    func itemDetailViewController(controller: ItemDetailViewController, didFinishEditingItem item: UrlItem) {
 
        if let index = topiclist.items.indexOf(item) {
            let indexPath = NSIndexPath(forRow: index, inSection: 0)
            if let cell = tableView.cellForRowAtIndexPath(indexPath) {
                configureTextForCell(cell, withUrlItem: item)
            }
        }
        dismissViewControllerAnimated(true, completion: nil)
        //saveUrllistItems()
    }
}

//MARK: - Persistence
//extension UrlListViewController
//{
//    func documentsDirectory() -> String {
//        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
//        return paths[0]
//    }
//    
//    func dataFilePath() -> String {
//        return (documentsDirectory() as NSString).stringByAppendingPathComponent("Urllists.plist")
//    }
//    
//    func saveUrllistItems() {
//        let data = NSMutableData()
//        let archiver = NSKeyedArchiver(forWritingWithMutableData: data)
//        archiver.encodeObject(items, forKey: "UrllistItems")
//        archiver.finishEncoding()
//        data.writeToFile(dataFilePath(), atomically: true)
//    }
//}

