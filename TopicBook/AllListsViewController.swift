//
//  AllListsViewController.swift
//  TopicBook
//
//  Created by SungJaeLEE on 2016. 1. 1..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit

class AllListsViewController: UITableViewController
{
    var dataModel: DataModel!

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowUrllist" {
            let controller = segue.destinationViewController as! UrlListViewController
            controller.topiclist = sender as! Topiclist
        } else if segue.identifier == "AddTopiclist" {
            let navigationController = segue.destinationViewController as! UINavigationController
            let controller = navigationController.visibleViewController as! ListDetailViewController
            controller.delegate = self
            controller.topiclistToEdit = nil
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        navigationController?.delegate = self
        
        let index = dataModel.indexOfSelectedTopiclist
        
        if index >= 0 && index < dataModel.lists.count {
            let topiclist = dataModel.lists[index]
            performSegueWithIdentifier("ShowUrllist", sender: topiclist)
        }
    }
}

//MARK: - TableviewDataSource..........
extension AllListsViewController
{
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.lists.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = cellForTableView(tableView)
        
        let topiclist = dataModel.lists[indexPath.row]
        cell.textLabel!.text = topiclist.name
        cell.accessoryType = .DetailDisclosureButton
        
        let count = topiclist.countUncheckedItems()
        if topiclist.items.count == 0 {
            cell.detailTextLabel!.text = "(No Items)"
        } else if count == 0 {
            cell.detailTextLabel!.text = "All Done!"
        } else {
            cell.detailTextLabel!.text = "\(count) Remaining"
        }
    
        cell.imageView!.image = UIImage(named: topiclist.iconName)
        return cell
    }
    
    func cellForTableView(tableView: UITableView) -> UITableViewCell {
        let cellIdentifier = "Cell"
        if let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) {
            return cell
        } else {
            return UITableViewCell(style: .Subtitle, reuseIdentifier: cellIdentifier)
        }
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        dataModel.lists.removeAtIndex(indexPath.row)
        
        let indexPaths = [indexPath]
        tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
    }
}

//MARK: - TableviewDelegate............
extension AllListsViewController
{
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        dataModel.indexOfSelectedTopiclist = indexPath.row
        
        let topiclist = dataModel.lists[indexPath.row]
        performSegueWithIdentifier("ShowUrllist", sender: topiclist)
    }
    
    override func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        let navigationController = storyboard!.instantiateViewControllerWithIdentifier("ListDetailNavigationController") as! UINavigationController
        let controller = navigationController.visibleViewController as! ListDetailViewController
        controller.delegate = self
        
        let topiclist = dataModel.lists[indexPath.row]
        controller.topiclistToEdit = topiclist
        
        presentViewController(navigationController, animated: true, completion: nil)
    }
}

//MARK: - Implement Protocol ListDetailViewControllerDelegate ..........
extension AllListsViewController: ListDetailViewControllerDelegate
{
    func listDetailViewControllerDidCancel(controller: ListDetailViewController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func listDetailViewController(controller: ListDetailViewController, didFinishAddingTopiclist topiclist: Topiclist) {
//        let newRowIndex = dataModel.lists.count
//        dataModel.lists.append(topiclist)
//        
//        let indexPath = NSIndexPath(forRow: newRowIndex, inSection: 0)
//        let indexPaths = [indexPath]
//        tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
            dataModel.lists.append(topiclist)
            dataModel.sortChecklists()
            tableView.reloadData()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func listDetailViewController(controller: ListDetailViewController, didFinishEditingTopiclist topiclist: Topiclist) {
//        if let index = dataModel.lists.indexOf(topiclist) {
//            let indexPath = NSIndexPath(forRow: index, inSection: 0)
//            if let cell = tableView.cellForRowAtIndexPath(indexPath) {
//                cell.textLabel!.text = topiclist.name
//            }
//        }
        dataModel.sortChecklists()
        tableView.reloadData()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}

//MARK: - UINavigationControllerDelegate............
extension AllListsViewController: UINavigationControllerDelegate
{
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        if viewController == self {
            dataModel.indexOfSelectedTopiclist = -1
        }
    }
}

